vagrant (2.3.7+git20230731.5fc64cde+dfsg-3) unstable; urgency=medium

  * Add patch 0012-Loosen-dependency-on-ruby.patch
    + Support Ruby 3.3. Closes: #1091664
  * Add patch 0013-Fix-the-default-vagrant-URL-for-pulling-boxes.patch
    + Fix incompatibility introduced by Hashicorp that prevented 
      the public registry to work with this version of Vagrant.
      Closes: #1092987

 -- Lucas Nussbaum <lucas@debian.org>  Fri, 24 Jan 2025 22:27:33 +0100

vagrant (2.3.7+git20230731.5fc64cde+dfsg-2) unstable; urgency=medium

  * Add patch: 0011-Add-newline-at-end-of-private-key.patch
    Fix a bug introduced in upstream commit 67562588 where invalid private
    keys were generated.

 -- Lucas Nussbaum <lucas@debian.org>  Tue, 07 May 2024 14:46:44 +0200

vagrant (2.3.7+git20230731.5fc64cde+dfsg-1) unstable; urgency=medium

  [ Antonio Terceiro ]
  * Drop myself from Uploaders

  [ Lucas Nussbaum ]
  * New upstream version 2.3.7+git20230731.5fc64cde+dfsg
    + This is the last commit before the license change to BUSL.
  * Add patch: 0009-Raise-error-if-openssl-headers-not-found.patch
  * Add build-dep on libssl-dev and switch to Architecture: any to build the
    vagrant_ssl extension
  * Fix rubygems-integration path due to the above arch:any change
  * Drop Build-dep on ruby-net-ftp, which is provided by libruby3.X anyway.
    Closes: #1070420
  * Remove watch file: we don't expect upstream updates
  * Add patch 0010-Remove-shebang-from-bash-completion-file.patch to fix
    lintian warning.

 -- Lucas Nussbaum <lucas@debian.org>  Mon, 06 May 2024 21:08:41 +0200

vagrant (2.3.6+dfsg-1) unstable; urgency=medium

  * New upstream version 2.3.6+dfsg
  * Add myself to Uploaders
  * Add patch: 0008-Skip-tests-that-require-rgl.patch

 -- Lucas Nussbaum <lucas@debian.org>  Tue, 05 Mar 2024 16:49:51 +0100

vagrant (2.3.5+dfsg-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Update standards version to 4.6.2, no changes needed.

  [ Lucas Nussbaum ]
  * New upstream version 2.3.5+dfsg
  * Refresh patches
  * Refresh packaging using dh-make-ruby -w

 -- Lucas Nussbaum <lucas@debian.org>  Tue, 05 Mar 2024 15:20:48 +0100

vagrant (2.3.4+dfsg-1) unstable; urgency=medium

  * New upstream version 2.3.4+dfsg
    - Passes its tests with rspec 3.12 (Closes: #1027098)
  * debian/control: update with dh-make-ruby
  * Refresh patches.
    Drop patch for VirtualBox 7.0 support as it is already present upstream.
  * test/unit/base.rb: don´t load patch to fake_ftp when running tests
  * debian/ruby-tests.rake: skip test that requires a rgl (not packaged)

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 30 Dec 2022 21:47:34 -0300

vagrant (2.2.19+dfsg-3) unstable; urgency=medium

  * Add upstream patch with VirtualBox 7.0 support (Closes: #1026227)

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 19 Dec 2022 21:53:30 -0300

vagrant (2.2.19+dfsg-2) unstable; urgency=medium

  [ Vincent Blut ]
  * d/vagrant.install: provide vagrant zsh completion

  [ Antonio Terceiro ]
  * Don't force an upper bound on Ruby versions (Closes: #1025261)

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 01 Dec 2022 16:25:04 -0300

vagrant (2.2.19+dfsg-1) unstable; urgency=medium

  * New upstream version 2.2.19+dfsg
    - Passes tests against ruby3.0 with ruby-rspec >= 3.10 (Closes: #996529)
  * debian/upstream/metadata: correct URLs
  * debian/control: refresh build dependencies
  * Bump Standards-Version to 4.6.0; no changes needed
  * Refresh patches
  * Refresh packaging files from dh-make-ruby
  * debian/rules: replace whitelist/blacklist with include/exclude
  * System-side installed plugins: cope with upstream changes
  * autopkgtest: integration-test: add missing dependency on dnsmaq
  * autopkgtest: integration-test: add missing dependency on sudo

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 15 Nov 2021 09:10:33 -0300

vagrant (2.2.14+dfsg-1) unstable; urgency=medium

  * New upstream version 2.2.14+dfsg
  * Refresh patches
  * Bump version constraints in Build-Depends
  * Run test suite on build and autopkgtest
    - Add a patch to skip a few tests that require winrm
    - Skip some entire test files that require winrm
    - Add new build dependencies: ruby-rspec, ruby-rspec-its, ruby-webmock,
      curl
    - Add patch to fix test suite to run against installed package

 -- Antonio Terceiro <terceiro@debian.org>  Sun, 07 Feb 2021 14:53:25 -0300

vagrant (2.2.9+dfsg-1) unstable; urgency=medium

  * Update package description to mention the fact that VirtualBox is not used
    by default (Closes: #960570)
  * New upstream version 2.2.9+dfsg
  * debian/control: packaging updates from a new dh-make-ruby run
  * Refresh patches against new upstream version
    - 0003-linux-cap-halt-don-t-wait-for-shutdown-h-now-to-fini.patch:
      dropped, issue has been fixed differently by upstream.
  * debian/tests/smoke-test: replace $ADTTMP with $AUTOPKGTEST_TMP
  * Drop lintian override for script-not-executable against bash completion
  * debian/tests/integration-test: add new dependencies

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 23 May 2020 09:03:43 -0300

vagrant (2.2.7+dfsg-1) unstable; urgency=medium

  * New upstream version 2.2.7+dfsg
  * Refresh packaging files with a new dh-make-ruby run
  * Refresh patches.
    Drop, already applied upstream:
    - 0006-nfs-avoid-adding-extra-newlines-to-etc-exports.patch
    - 0007-Add-VirtualBox-provider-support-for-version-6.1.x.patch
  * Add patches for ruby2.7 support

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 21 Mar 2020 21:45:45 -0300

vagrant (2.2.6+dfsg-2) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Antonio Terceiro ]
  * Add patch to avoid /etc/exports growing forever
  * Add patch for VirtualBox 6.1 support (Closes: #946837)
  * Add Breaks: on unsupported VirtualBox versions (Closes: #946838)

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 24 Dec 2019 16:54:36 -0300

vagrant (2.2.6+dfsg-1) unstable; urgency=medium

  * New upstream version 2.2.6+dfsg
  * Refresh patches
  * Bump dependency on ruby-vagrant-cloud to >= 2.0.3
  * autopkgtest: don't try running a box under /tmp
  * Bump Standards-Version to 4.4.1; no changes needed
  * autopkgtest: drop obsolete resstriction needs-recommends
  * debian/rules: drop README.md files outside of doc dirs
  * autopkgtest smoke-test: use vagrant-libvirt

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 14 Nov 2019 10:13:03 -0300

vagrant (2.2.3+dfsg-1) unstable; urgency=medium

  * New upstream version 2.2.3+dfsg
    - adds support for Virtualbox 6 (Closes: #921036)
  * Rebase patches
  * Update packaging with new run of dh-make-ruby
    - Bump debhelper compat to 11
    - Update dependencies
    - Bump Standards-Version to 4.3.0, no changes needed otherwise
    - Update Homepage: to an https:// URL
    - Add Rules-Require-Root: no
    - wrap-and-sort

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 05 Feb 2019 16:57:54 -0200

vagrant (2.1.5+dfsg-1) unstable; urgency=medium

  [ Antonio Terceiro ]
  * New upstream release
  * Refresh patches
  * Add missing dependency on rsync (Closes: #896660)
  * Bump dependency on ruby-net-ssh to >= 1:5, as vagrant now uses features
    that are not present before that version.

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 02 Oct 2018 09:28:07 -0300

vagrant (2.1.2+dfsg-1) unstable; urgency=medium

  * New upstream version 2.1.2+dfsg (Closes: #906444)
  * Refresh patches
  * debian/{copyright, control}: point to https://github.com/hashicorp
  * Change dependency from bsdtar to libarchive-tools (Closes: #900489)
  * Bump Standards-Version to 4.2.0; no changes needed

 -- Antonio Terceiro <terceiro@debian.org>  Sun, 26 Aug 2018 15:51:01 -0300

vagrant (2.0.2+dfsg-3) unstable; urgency=medium

  * Specify minimal version of ruby-net-ssh required (Closes: #890662)

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 26 Mar 2018 16:14:19 -0300

vagrant (2.0.2+dfsg-2) unstable; urgency=medium

  * New upstream release.
    - Removes usage of setting name deprecated by ruby-net-ssh (Closes: #884104)
  * debian/tests/integration-test: make sure box pulled for testing is a
    Debian one to avoid failures on non-Debian hosts (Closes: #889174)
  * Refresh patches:
    - 0001-Disable-Checkpoint.patch: removed as checkpoint integration was
      dropped upstream
    - 0007-Virtualbox-5.2-support.patch: removed, applied upstream
    - Renumbered remaining patches
  * Bump Standards-Version to 4.1.3; no changes needed
  * Change Maintainer to the Debian Ruby Team
  * Point Vcs-* to salsa.debian.org
  * debian/rules: check dependencies during the build
    - added dependencies as build dependencies
  * debian/vagrant.lintian-overrides: override warning about shebang in
    upstream bash completion file

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 03 Feb 2018 10:44:25 -0200

vagrant (2.0.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.0.0+dfsg
  * 0007-Virtualbox-5.2-support.patch: apply upstream patch adding support for
    VirtualBox 4.2 (Closes: #879931)

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 28 Oct 2017 11:31:32 -0200

vagrant (1.9.8+dfsg-1) unstable; urgency=medium

  * New upstream release
  * Refresh patches
    - 0007-Update-command.rb.patch removed, already applied upstream
  * Recommends: vagrant-libvirt (Closes: #855372)
    - given virtualbox wasn't released in stretch (#794466), vagrant-libvirt is
      the provider we have that will work out of the box. Of course, this
      assumes that the box you want to use is available for libvirt as well.
  * Drop unnecessary dependency on ruby-bundler (See #838213). The code that
    uses it has already been dropped in 1.9.0+dfsg-1
  * debian/README.Debian: update documentation on packaging vagrant plugins
    (see #838898)
  * debian/tests/integration-test: new autopkgtest integration test script
    that verifies that vagrant can bring up a Debian stable VM out of the box.
  * Add a manpage for dh_vagrant_plugin.
  * Bump debhelper compatibility level to 10
  * Bump Standards-Version to 4.0.0, no changes needed
  * Drop dependencies that were also dropped upstream:
    - ruby-nokogiri
    - ruby-rb-inotify

 -- Antonio Terceiro <terceiro@debian.org>  Wed, 06 Sep 2017 14:51:22 -0300

vagrant (1.9.1+dfsg-1) unstable; urgency=medium

  * New upstream version 1.9.1+dfsg
  * Refresh patches
  * 0007-Update-command.rb.patch: fix `vagrant package` (Closes: 849723)

 -- Antonio Terceiro <terceiro@debian.org>  Sun, 15 Jan 2017 22:11:47 -0200

vagrant (1.9.0+dfsg-2) unstable; urgency=medium

  * Fix regression in dh_vagrant_plugin (Closes: #847548)

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 09 Dec 2016 07:49:04 -0200

vagrant (1.9.0+dfsg-1) unstable; urgency=medium

  * New upstream release
    - includes fix for issue with permissions when with inserting
      dynamically-generated SSH key into boxes (Closes: #838533)
  * debian/rules: install package using rubygems layout (dh_ruby
    --gem-install). Together with the fact that vagrant upstream dropped the
    very tight dependency on bundler, this allows us to drop the following
    patches:
    + 0002-VERSION-fallback-to-usr-share-vagrant-version.txt.patch
    + 0004-Read-data-from-usr-share-vagrant.patch
    + 0005-Look-up-vagrant-pre-rubygems.rb-from-the-installed-p.patch
    + 0006-Make-Bundler-also-lookup-into-rubygems-integration-p.patch
    + 0010-require-vagrant-version-from-system.patch
    + 0012-plugins-don-t-abuse-require_relative.patch
  * Refreshed and renumbered remaining patches
  * 0001-Disable-Checkpoint.patch: remove further checkpoint usage from
    `vagrant version`
  * Bump debhelper compatibility level to 9

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 08 Dec 2016 11:23:19 -0200

vagrant (1.8.5+dfsg-2) unstable; urgency=medium

  * 0012-plugins-don-t-abuse-require_relative.patch: fix regression introduced
    on latest update. Thanks to Fumiyasu SATOH (Closes: #836229)

 -- Antonio Terceiro <terceiro@debian.org>  Wed, 31 Aug 2016 18:11:06 -0300

vagrant (1.8.5+dfsg-1) unstable; urgency=medium

  * New upstream release
    - Add support for VirtualBox 5.1 (Closes: #835410)
  * Refresh patches

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 25 Aug 2016 15:29:04 -0300

vagrant (1.8.4+dfsg-2) unstable; urgency=medium

  * Depends: require bundler >= 1.12.5 (Closes: #832424)
  * Vcs-Browser: use https URL
  * Bump Standards-Version to 3.9.8; no changes needed otherwise

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 25 Jul 2016 12:00:27 -0300

vagrant (1.8.4+dfsg-1) unstable; urgency=medium

  * New upstream release
    - Refresh patches
    - 0012-Fix-Vagrant-Bundler-to-work-against-Rubygems-from-Ru.patch:
      dropped, already applied upstream
    - Add new patch 0012-plugins-don-t-abuse-require_relative.patch
  * change dependency from `bundler` to `ruby-bundler` to avoid pulling in a
    toolchain
  * debian/tests/smoke-test: remove plugin installation test since that
    depends on having a toolchain installed, because vagrant plugin
    installation will re-install stuff that has already been installed via
    APT.
  * debian/rules: check for require_relative abuses under plugins/ in
    override_dh_auto_test

 -- Antonio Terceiro <terceiro@debian.org>  Wed, 29 Jun 2016 14:50:30 -0300

vagrant (1.8.1+dfsg-2) unstable; urgency=medium

  [ Christian Hofstaedtler ]
  * Fix Vagrant::Bundler to work against Rubygems from Ruby 2.3
    (Closes: #817014)

  [ Antonio Terceiro ]
  * debian/tests/smoke-test: add simple plugin installation step to smoke test
    to catch any future issues with that.
  * Renumber patches
  * Bump Standards-Version to 3.9.7, no changes needed
  * Change Vcs-* to use https URLs

 -- Antonio Terceiro <terceiro@debian.org>  Sun, 03 Apr 2016 18:57:57 -0300

vagrant (1.8.1+dfsg-1) unstable; urgency=medium

  * New upstream release.
    - refresh patches
  * debian/control: swap me and Laurent as Maintainer/Uploader to reflect
    reality.
  * 0008-Use-a-private-temporary-dir.patch: update to remove remaining usage
    of several different temporary directories; use a central temporary
    directory instead, and clean it up when the application exits
    (Closes: #811004)

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 21 Jan 2016 09:40:46 -0200

vagrant (1.7.4+dfsg-1) unstable; urgency=medium

  * New upstream release
    - adds support for VirtualBox 5.0 (Closes: #795666)
  * Refresh patches

 -- Antonio Terceiro <terceiro@debian.org>  Sun, 16 Aug 2015 11:04:06 +0200

vagrant (1.7.2+dfsg-5) unstable; urgency=medium

  * 0012-require-vagrant-version-from-system.patch: require 'vagrant/version'
    directly on --version (Closes: #788494)

 -- Antonio Terceiro <terceiro@debian.org>  Fri, 07 Aug 2015 09:28:08 -0300

vagrant (1.7.2+dfsg-4) unstable; urgency=medium

  * Refresh patches
  * Support-system-installed-plugins.patch: another attempt at system-provided
    plugins. Closes: #740165
    - Install an empty /usr/share/vagrant-plugins/plugins.json to force
      looking for plugins in /usr/share/vagrant-plugins/plugins.d
    - Vagrant plugins must be packaged as regular Ruby libraries + a few extra
      bits -- see README.Debian for details
    - Provide a dh_vagrant_plugin helper that can be used to create those
      extra bits automatically -- see README.Debian for details

 -- Antonio Terceiro <terceiro@debian.org>  Wed, 27 May 2015 14:23:39 -0300

vagrant (1.7.2+dfsg-3) unstable; urgency=medium

  * 0003-VERSION-fallback-to-usr-share-vagrant-version.txt.patch: fix reading
    version number (Closes: #785631)

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 26 May 2015 18:23:50 -0300

vagrant (1.7.2+dfsg-2) experimental; urgency=medium

  * 0010-provisioners-puppet-fix-exception-with-module-paths-.patch:
    fix crash with pupper provisioner. Closes: #779637

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 03 Mar 2015 11:30:53 -0300

vagrant (1.7.2+dfsg-1) experimental; urgency=medium

  * New upstream release
    - Refreshed patches
  * New dependency: ruby-net-sftp
  * New dependencies: ruby-rest-client
  * 0009-linux-cap-halt-don-t-wait-for-shutdown-h-now-to-fini.patch:
    do not wait for `shutdown -h now` to return; this fixes `vagrant halt`
    with lxc guests (using the vagrant-lxc plugin)

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 09 Feb 2015 12:19:27 -0200

vagrant (1.6.5+dfsg1-2) unstable; urgency=medium

  * debian/patches/0008-Use-a-private-temporary-dir.patch: create temporary
    files inside a private temporary directory instead of cluttering $TMPDIR.
  * debian/patches/0003-VERSION-fallback-to-usr-share-vagrant-version.txt.patch:
    updated, remove trailing newline from version number (Closes: #765074)
    - Thanks Etienne Millon <me@emillon.org>

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 28 Oct 2014 12:12:27 -0200

vagrant (1.6.5+dfsg1-1) unstable; urgency=medium

  [ Antonio Terceiro ]
  * New upstream release (Closes: #741478)
    + Thanks to Stig Sandbeck Mathisen, Balint Reczey, and Sebastien Badia
      for our previous more-or-less joint effort in trying to package post-1.4
      vagrant releases. I decided to restart fresh off the state in the 1.4
      branch, but their efforts are appreciated.
    + Required quite some patching to make it work as a Debian package, since
      the new upstream version relies quite heavily on either being installed
      by its own installer, or being run from the source tree. See
      debian/patches/ for details.
  * debian/control: added new dependencies
    + bundler
    + ruby-listen
    + ruby-nokogiri
    + ruby-rb-inotify
  * Added a DEP-8 smoke test to check dependencies and basic functionality
  * debian/rules: ignore the `require-rubygems` test

  [ Sebastien Badia ]
  * d/vagrant.bash-completion: Use upstream bash-completion file

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 11 Oct 2014 12:19:54 -0300

vagrant (1.4.3+dfsg1-3) unstable; urgency=medium

  * Drop dependency on ruby-net-ssh (<< something). Let's assume it will work
    with anything newer than the minimum required version.

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 06 Oct 2014 15:20:27 -0300

vagrant (1.4.3+dfsg1-2) unstable; urgency=medium

  * Drop debian/patches/03_debian_plugins.patch as it breaks installing
    plugins from via vagrant official mechanism (vagrant plugin install)
    Closes: #758033

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 14 Aug 2014 11:21:07 -0300

vagrant (1.4.3+dfsg1-1) unstable; urgency=medium

  * Repack upstream tarball to remove website/ directory (which is useless for
    us, anyway) containing sourceless Javascript files (Closes: #736787)
  * debian/patches/03_debian_plugins.patch: support vagrant plugins installed
    as Debian packages, to unblock packaging of vagrant plugins
    (Closes: #740165)
    .
    We will have to figure out how to forward port this patch to newer
    vagrant releases later.

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 07 Aug 2014 11:00:23 -0300

vagrant (1.4.3-2) unstable; urgency=medium

  * Use the default Ruby interpreter instead of hardcoding ruby1.9.1
    (Closes: #744033)
  * Add myself to Uploaders:

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 12 May 2014 11:50:35 -0300

vagrant (1.4.3-1) unstable; urgency=low

  [ Sebastien Badia ]
  * New upstream release (Closes: #735150, #732155)
  * Add a manpage for vagrant (Closes: #710193)

  [ Laurent Bigonville ]
  * Downgrade virtualbox from Recommends to Suggests (Closes: #724903)
  * debian/control: Add maximum supported version for ruby-net-ssh

 -- Laurent Bigonville <bigon@debian.org>  Thu, 23 Jan 2014 00:26:47 +0100

vagrant (1.2.2-1) unstable; urgency=low

  * New upstream release (Closes: #697280)
    - Add support for VirtualBox 4.2 (Closes: #710242)
    - Vagrant now only works with ruby 1.9 (Closes: #702144)
    - debian/control: Adjust dependencies
    - debian/patches/01_source_root_path.patch: Refreshed
  * debian/watch: Update watch file URL, vagrant is no longer distributed as a
    gem (Closes: #711110)
  * debian/control: Use canonical URL for VCS fields
  * debian/control: Add bsdtar, curl and openssh-client to the dependencies
  * debian/control: Bump Standards-Version to 3.9.4 (no further changes)
  * debian/vagrant.install: Install plugins directory
  * debian/patches/02_disable_installer_warning.patch: Disable warning about
    not using the installer
  * debian/require-rubygems.overrides: Refresh file list
  * debian/vagrant.bash-completion: Add completion for the new arguments

 -- Laurent Bigonville <bigon@debian.org>  Sat, 06 Jul 2013 21:47:22 +0200

vagrant (1.0.3-1) unstable; urgency=low

  * New upstream release
  * debian/vagrant.bash-completion: Add bash completion file

 -- Laurent Bigonville <bigon@debian.org>  Thu, 03 May 2012 17:01:09 +0200

vagrant (1.0.2-1) unstable; urgency=low

  * New upstream release
  * debian/patches/01_source_root_path.patch: Refreshed

 -- Laurent Bigonville <bigon@debian.org>  Thu, 05 Apr 2012 12:13:23 +0200

vagrant (1.0.1-1) unstable; urgency=low

  * New upstream release
  * debian/require-rubygems.overrides: Add lib/vagrant/command/gem.rb,
    lib/vagrant/environment.rb
  * debian/control:
    - Bump Standards-Version to 3.9.3 (no further changes)
    - Add version to rubygems dependency

 -- Laurent Bigonville <bigon@debian.org>  Wed, 14 Mar 2012 11:48:07 +0100

vagrant (0.9.7-1) unstable; urgency=low

  * New upstream release
    - Bump ruby-childprocess dependency to 0.3.1
  * Add debian/gbp.conf file

 -- Laurent Bigonville <bigon@debian.org>  Fri, 17 Feb 2012 11:19:55 +0100

vagrant (0.9.5-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Drop DM-Upload-Allowed

 -- Laurent Bigonville <bigon@debian.org>  Mon, 06 Feb 2012 11:24:58 +0100

vagrant (0.9.3-1) unstable; urgency=low

  * Initial release (Closes: #628031)

 -- Laurent Bigonville <bigon@debian.org>  Sat, 28 Jan 2012 12:36:35 +0100
