require 'gem2deb/rake/spectask'
require 'tmpdir'

SKIP = %w[
  test/unit/vagrant/action/builtin/box_add_test.rb
  test/unit/plugins/communicators/winrm/*_test.rb
  test/unit/plugins/pushes/ftp/*_test.rb
  test/unit/plugins/commands/serve/mappers_test.rb
]

lib = File.expand_path(File.dirname(__FILE__) + "/../debian/lib")

ENV["HOME"] = Dir.mktmpdir("vagrant-tests-home")

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = 'test/unit/{plugins}/**/*_test.rb'
  spec.exclude_pattern = "{#{SKIP.join(',')}}"
  spec.rspec_opts = "-I#{lib}"
end
